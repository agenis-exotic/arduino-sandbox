Il s'agit d'un projet pour afficher les infos du dernier tweet d'une page donnée, sur une matrice LED 8x8. 
Matériel:
- raspberry pi 4B avec install vierge full 32bits (du 2021-01-11)
- 1 Module arduino max7219 avec son multiplexeur dessous
- les cables dupond pour le connecter (VCC-pin2, GND-pin6, DIN-pin19, CS-pin24, CLK-pin23)

gestion souris ajouter ca à la fin de la ligne de: `/boot/cmdline.txt`

`usbhid.mousepoll=0`

le tuto original ici: https://luma-led-matrix.readthedocs.io/en/latest/install.html

`sudo apt-get update`

`sudo apt-get install chromium-chromedriver`

`pip3 install selenium`

=> autoriser SPI:

`sudo raspi-config `

`sudo usermod -a -G spi,gpio pi`

`sudo apt install build-essential libfreetype6-dev libjpeg-dev libopenjp2-7 libtiff5`

`sudo -H pip install --upgrade luma.led_matrix`

`cd /home/pi/Documents/mkdir twitter-leds`

`cd twitter-leds/`

`git clone https://github.com/rm-hull/luma.led_matrix.git`

`cd luma.led_matrix`

tester si ca marche: `python3 examples/matrix_demo.py`



ajouter dans crontab:

`crontab -e`

`@reboot /usr/bin/python3 /home/pi/Documents/twitter-leds/luma.led_matrix/examples/view_message_twitter.py &`

(adapter le chemin si besoin)

![image](images/device.png)


