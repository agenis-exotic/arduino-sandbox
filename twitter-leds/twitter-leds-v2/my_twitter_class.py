#!/usr/bin/env python
# -*- coding: utf-8 -*-

from auth import *
import requests
import os
import json
from view_message_twitter_v2 import mylogger

class my_twitter_class():
    def __init__(self, user_id):
        mytwitter_fields = "created_at,public_metrics,referenced_tweets"
        self.Bearer_token = Bearer_token
        self.user_id = user_id
        self.url = "https://api.twitter.com/2/users/{}/tweets".format(self.user_id)
        self.params = {'tweet.fields': mytwitter_fields, 'max_results': 5}
        self.headers = {"Authorization": "Bearer {}".format(self.Bearer_token)}
        mylogger.logEvent("twitter api instance created")

    def connect_and_request(self):
        response = requests.request("GET", self.url, headers=self.headers, params=self.params)
        mylogger.logEvent("twitter api request send status: {}".format(response.status_code))
        if response.status_code != 200:
            mylogger.logEvent("error with twitter api request")
            raise Exception(
                "Request returned an error {} {}".format(
                    response.status_code, response.text)
                   )
        with open('last_response_api.txt', 'w') as outfile:
            json.dump(response.json(), outfile)
        self.response = response.json()

    def get_metrics(self):
        for possible_last_tweet in self.response["data"]:
            if "referenced_tweets" in possible_last_tweet:
                # si le champ referenced_tweets est présent, alors le tweet n'est pas original de la personne.
                # alors on n'acceptue que les citations:
                if possible_last_tweet["referenced_tweets"] == "quoted":
                    break
            else:
                break
        last = possible_last_tweet["public_metrics"]
        metrics = [last["reply_count"], last["quote_count"]+last["retweet_count"], last["like_count"]]
        mylogger.logEvent("correctly computed metrics")
        return metrics


if __name__ == "__main__":
    twt = my_twitter_class(1077900401029066753)
    twt.connect_and_request()
    output = twt.get_metrics()
    print(output)

    
