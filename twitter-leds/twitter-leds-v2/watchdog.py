#! /usr/bin/env python3.7
# -*- coding: utf-8


from threading import Timer
import time
import os
from view_message_twitter_v2 import mylogger

def my_reboot_handler():
    mylogger.logEvent("watchdog timed out. Rebooting in 2 seconds")
    mylogger.shutdown()
    time.sleep(2)
    os.system("sudo reboot")

class Watchdog(Exception):
    def __init__(self, timeout, userhandler=None, custom_name="wtg0"):
        self.custom_name = custom_name
        self.timeout = timeout
        self.handler = userhandler
        self.timer = Timer(self.timeout, self.handler)
        self.timer.start()
        mylogger.logEvent("starting watchdog " + self.custom_name)

    def reset(self):
        mylogger.logEvent("reseting watchdog " + self.custom_name)
        self.timer.cancel()
        self.timer = Timer(self.timeout, self.handler)
        self.timer.start()

    def stop(self):
        mylogger.logEvent("stopping watchdog " + self.custom_name)
        self.timer.cancel()

    def defaultHandler(self):
        raise self

if __name__ == "__main__":
    wdg = Watchdog(5, my_reboot_handler)
    time.sleep(4)
    wdg.reset()

