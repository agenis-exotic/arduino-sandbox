#!/usr/bin/env python
# -*- coding: utf-8 -*-


import time
import argparse
from luma.led_matrix.device import max7219
from luma.core.interface.serial import spi, noop
from luma.core.legacy import show_message
from luma.core.legacy.font import proportional, CP437_FONT
from twitter_fonts import *
import time
from threading import Thread
from myloggerclass import *

global twitter_text_to_display

mylogger = MyLogger()

from my_twitter_class import *
from watchdog import *

def output_to_leds(n, block_orientation, rotate, inreverse, text):
    # create matrix device
    serial = spi(port=0, device=0, gpio=noop())
    device = max7219(serial, cascaded=n or 1, block_orientation=block_orientation,
                     rotate=rotate or 0, blocks_arranged_in_reverse_order=inreverse)
    mylogger.logEvent("created led matrix device instance.")
    device.contrast(4)
    show_message(device, text, fill="white", font=proportional(TWITTER_FONT), scroll_delay=0.05)
    time.sleep(1)

def get_last_tweet_infos(twitter_class_instance):
    global twitter_text_to_display
    twitter_class_instance.connect_and_request()
    output = twitter_class_instance.get_metrics()
    twitter_text_to_display = "C{} R{} L{}".format(output[0], output[1], output[2])
    mylogger.logEvent("updated text to display")
    time.sleep(30)
    wdg2.reset()

def get_last_tweet_infos_loop():
    global twitter_text_to_display
    while True:
        try:
            get_last_tweet_infos(twt)
        except:
            pass

def display_loop_matrix():
    global twitter_text_to_display
    while True:
        wdg1.reset()
        show_message(device, twitter_text_to_display, fill="white", font=proportional(TWITTER_FONT), scroll_delay=0.05)

if __name__ == "__main__":
    wdg1 = Watchdog(60*2, my_reboot_handler, "leds_watchdog")
    wdg2 = Watchdog(60*10, my_reboot_handler, "twitter_api_watchdog")
    # create led matrix device
    serial = spi(port=0, device=0, gpio=noop())
    device = max7219(serial, cascaded=1, block_orientation=0, rotate=0, blocks_arranged_in_reverse_order=False)
    device.contrast(4)
    mylogger.logEvent("created led matrix device instance.")
    twitter_text_to_display = "C? R? L?"
    twt = my_twitter_class(1077900401029066753)
    t1 = Thread(target=get_last_tweet_infos_loop)
    t2 = Thread(target=display_loop_matrix)
    mylogger.logEvent("Starting threads.")
    t1.start()
    t2.start()

# a mettre dans crontab:
# @reboot /usr/bin/python3 /home/pi/Documents/twitter-leds-v2/view_message_twitter_v2.py &
