#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

import datetime
import os

FILENAME = "/home/pi/Documents/twitter-leds/logs/file.log"

class MyLogger:
    def __init__(self):
        if not os.path.exists("/home/pi/Documents/twitter-leds/logs/"): os.mkdir("/home/pi/Documents/twitter-leds/logs/")
        # os.remove(FILENAME) # remove previous logs
        self.target = open(FILENAME, "a+")
        self.target.write("new log created\n")
        
    def logEvent(self, event):
        currenttime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        output = '[' + currenttime + '] ' + str(event)
        self.target.write(output + "\n")
        self.target.flush()
    
    def shutdown(self):
        self.logEvent("saving and closing log...\n\n\n\n")
        self.target.close()

if __name__ == "__main__":
    mylogger = MyLogger()
    mylogger.logEvent("Hello World!")
    mylogger.shutdown()