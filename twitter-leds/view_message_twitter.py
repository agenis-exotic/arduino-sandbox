#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Commandline Wrapper
# Thomas Wenzlaff
# See LICENSE.rst for details.

import time
import argparse
from luma.led_matrix.device import max7219
from luma.core.interface.serial import spi, noop
from luma.core.legacy import show_message
from luma.core.legacy.font import proportional, CP437_FONT
from twitter_fonts import *
import re
from selenium import webdriver
import time
from threading import Thread

global twitter_text_to_display

def output(n, block_orientation, rotate, inreverse, text):
    # create matrix device
    serial = spi(port=0, device=0, gpio=noop())
    device = max7219(serial, cascaded=n or 1, block_orientation=block_orientation,
                     rotate=rotate or 0, blocks_arranged_in_reverse_order=inreverse)
    device.contrast(4)
    print(text)

    show_message(device, text, fill="white", font=proportional(TWITTER_FONT), scroll_delay=0.05)
    time.sleep(1)

def get_last_tweet_infos():
    global twitter_text_to_display
    driver.get(url)
    # Wait to load page
    time.sleep(15)
    html = driver.page_source
    myregex = "(?:(?:([0-9]*) réponses*, )*([0-9]*) Retweets, )*([0-9]*) J'aime"
    result = re.search(myregex, html)
    if result.group(0) is None:
        print("not found...")
        twitter_text_to_display = "C? R? L?"
    else:
        print("found!")
        numbers = [int(s) for s in result.group(0).split() if s.isdigit()]
        print(numbers)
        while(len(numbers) < 3):
            numbers.insert(0, 0)
        twitter_text_to_display = "C{} R{} L{}".format(numbers[0], numbers[1], numbers[2])
    time.sleep(5)

def get_last_tweet_infos_loop():
    global twitter_text_to_display
    while True:
        try:
            get_last_tweet_infos()
        except:
            pass
        
def display_loop_matrix():
    global twitter_text_to_display
    while True:
        output(1, 0, 0, False, twitter_text_to_display)

if __name__ == "__main__":
    twitter_text_to_display = "C? R? L?"

    options = webdriver.ChromeOptions()
    options.add_argument("-headless")
    options.add_argument('--ignore-certificate-errors')
    driver = webdriver.Chrome(options=options)
    url = 'https://twitter.com/Frexit_Memes'
    driver.implicitly_wait(5)
    
    t1 = Thread(target=get_last_tweet_infos_loop)
    t2 = Thread(target=display_loop_matrix)
    t1.start()
    t2.start()

# a mettre dans crontab:
# @reboot /usr/bin/python3 /home/pi/Documents/twitter-leds/luma.led_matrix/examples/view_message_twitter.py &
