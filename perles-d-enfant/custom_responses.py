# -*- coding: utf-8 -*-

# This file stores the text responses of the skill
WELCOME_MESSAGE_FR = "Bienvenue dans nos perles d'enfants! Voulez-vous entendre une perle choisie au hasard?"
WELCOME_PROMPT_FR = "Voulez-vous entendre une perle choisies au hasard?"
HELP_MESSAGE_FR = "Dites 'encore une' pour écouter une perle au hasard"
BYE_MESSAGE_FR = "Vous êtes vraiment trop drôles! A bientôt les enfants!"
ERROR_MESSAGE_FR = "Désolée, je n'ai pas compris. Dites 'encore une perle' pour ecouter d'autres perles ou 'stop' pour quitter."
ASKAGAIN_MESSAGE_FR = "Encore une?"

