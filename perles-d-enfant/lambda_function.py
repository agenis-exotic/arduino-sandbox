# -*- coding: utf-8 -*-

# This sample demonstrates handling intents from an Alexa skill using the Alexa Skills Kit SDK for Python.
# Please visit https://alexa.design/cookbook for additional examples on implementing slots, dialog management,
# session persistence, api calls, and more.
# This sample is built using the handler classes approach in skill builder. 
import logging
#import boto3
import os
import json
import random # random.randint(3, 9) # nombre entier entre 3 et 9

import ask_sdk_core.utils as ask_utils

from ask_sdk_core.skill_builder import SkillBuilder
from ask_sdk_core.skill_builder import CustomSkillBuilder
from ask_sdk_core.dispatch_components import AbstractRequestHandler
from ask_sdk_core.dispatch_components import AbstractExceptionHandler
from ask_sdk_core.handler_input import HandlerInput

from ask_sdk_model import Response

# from ask_sdk_dynamodb.adapter import DynamoDbAdapter # pour parler a la base de donnees

from custom_responses import *

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


# ouvrir le lien vers la BDD
#ddb_region = os.environ.get('DYNAMODB_PERSISTENCE_REGION')
#ddb_table_name = os.environ.get('DYNAMODB_PERSISTENCE_TABLE_NAME')
#ddb_resource = boto3.resource('dynamodb', region_name=ddb_region)
#dynamodb_adapter = DynamoDbAdapter(table_name=ddb_table_name, create_table=False, dynamodb_resource=ddb_resource)


# ouvrir le json avec les citations en local... :-(
with open('citations.json') as json_file:
    citations = json.load(json_file)

class LaunchRequestHandler(AbstractRequestHandler):
    """Handler for Skill Launch."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_request_type("LaunchRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = WELCOME_MESSAGE_FR
        return (
            handler_input.response_builder
                .speak(speak_output)
                .ask(speak_output)
                .response
        )


# MON SUPER INTENT!
class SpeakRandomPerleIntentHandler(AbstractRequestHandler):
    """Handler for Random Perle Intent."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("SpeakRandomPerleIntent")(handler_input)

    def handle(self, handler_input):
        #persistent_attributes = handler_input.attributes_manager.persistent_attributes
        phrases = citations[random.randint(0, len(citations)-1)]["citation"]['SS']
        pause = "<break time='0.5s'/>"
        pause2 = "<break time='1s'/>"
        speak_output = pause.join(phrases) + pause
        speak_output += pause2 + ASKAGAIN_MESSAGE_FR
        # type: (HandlerInput) -> Response
        handler_input.response_builder.speak(speak_output).ask(ASKAGAIN_MESSAGE_FR)
        return handler_input.response_builder.response


# reponse OUI
class ReponseOuiIntent(AbstractRequestHandler):
    """Handler for Random Perle Intent."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("ReponseOui")(handler_input)
    def handle(self, handler_input):
        return SpeakRandomPerleIntentHandler().handle(handler_input)


# reponse Non
class ReponseNonIntent(AbstractRequestHandler):
    """Handler for Random Perle Intent."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("ReponseNon")(handler_input)
    def handle(self, handler_input):
        return handler_input.response_builder.speak(BYE_MESSAGE_FR).set_should_end_session(True).response


class HelpIntentHandler(AbstractRequestHandler):
    """Handler for Help Intent."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("AMAZON.HelpIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = HELP_MESSAGE_FR

        return (
            handler_input.response_builder
                .speak(speak_output)
                .ask(speak_output)
                .response
        )


class CancelOrStopIntentHandler(AbstractRequestHandler):
    """Single handler for Cancel and Stop Intent."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return (ask_utils.is_intent_name("AMAZON.CancelIntent")(handler_input) or
                ask_utils.is_intent_name("AMAZON.StopIntent")(handler_input))

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = BYE_MESSAGE_FR

        return (
            handler_input.response_builder
                .speak(speak_output)
                .response
        )


class SessionEndedRequestHandler(AbstractRequestHandler):
    """Handler for Session End."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_request_type("SessionEndedRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response

        # Any cleanup logic goes here.

        return handler_input.response_builder.response


class FallbackIntentHandler(AbstractRequestHandler):
    """Handler for fallback intent.
    2018-July-12: AMAZON.FallbackIntent is currently available in all
    English locales. This handler will not be triggered except in that
    locale, so it can be safely deployed for any locale. More info
    on the fallback intent can be found here: https://developer.amazon.com/docs/custom-skills/standard-built-in-intents.html#fallback
    """
    def can_handle(self, handler_input):
        return is_intent_name("AMAZON.FallbackIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("In FallbackIntentHandler")
        speak_output = ERROR_MESSAGE_FR
        return handler_input.response_builder.speak(speak_output).ask(
            speak_output).response


class IntentReflectorHandler(AbstractRequestHandler):
    """The intent reflector is used for interaction model testing and debugging.
    It will simply repeat the intent the user said. You can create custom handlers
    for your intents by defining them above, then also adding them to the request
    handler chain below.
    """
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_request_type("IntentRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        intent_name = ask_utils.get_intent_name(handler_input)
        speak_output = "You just triggered " + intent_name + "."

        return (
            handler_input.response_builder
                .speak(speak_output)
                # .ask("add a reprompt if you want to keep the session open for the user to respond")
                .response
        )


class CatchAllExceptionHandler(AbstractExceptionHandler):
    """Generic error handling to capture any syntax or routing errors. If you receive an error
    stating the request handler chain is not found, you have not implemented a handler for
    the intent being invoked or included it in the skill builder below.
    """
    def can_handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> bool
        return True

    def handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> Response
        logger.error(exception, exc_info=True)

        speak_output = ERROR_MESSAGE_FR

        return (
            handler_input.response_builder
                .speak(speak_output)
                .ask(speak_output)
                .response
        )

# The SkillBuilder object acts as the entry point for your skill, routing all request and response
# payloads to the handlers above. Make sure any new handlers or interceptors you've
# defined are included below. The order matters - they're processed top to bottom.


sb = SkillBuilder() # CustomSkillBuilder(persistence_adapter = dynamodb_adapter)
# sb = CustomSkillBuilder(persistence_adapter = dynamodb_adapter)

sb.add_request_handler(LaunchRequestHandler())
sb.add_request_handler(HelpIntentHandler())
sb.add_request_handler(CancelOrStopIntentHandler())
sb.add_request_handler(SessionEndedRequestHandler())
sb.add_request_handler(SpeakRandomPerleIntentHandler()) # MON INTENT!
sb.add_request_handler(ReponseOuiIntent())
sb.add_request_handler(ReponseNonIntent())
sb.add_request_handler(FallbackIntentHandler())
sb.add_request_handler(IntentReflectorHandler()) # make sure IntentReflectorHandler is last so it doesn't override your custom intent handlers


sb.add_exception_handler(CatchAllExceptionHandler())

lambda_handler = sb.lambda_handler()
