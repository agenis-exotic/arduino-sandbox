# Scanner de bluetooth vers une BDD cloud

Attention, afin d'éviter de lancer le script en sudo, il faut exécuter ces lignes, une fois installée la librairie bluepy:

`pip3 install bluepy`

`sudo setcap cap_net_raw+e /usr/local/lib/python3.7/dist-packages/bluepy/bluepy-helper`

`sudo setcap cap_net_admin+eip /usr/local/lib/python3.7/dist-packages/bluepy/bluepy-helper`

Ensuite on peut ajouter le script au crontab

`crontab -e`

ajouter a la fin:

`\*/1 * * * * /usr/bin/python3 /home/pi/Documents/scan.py >> /home/pi/Documents/scan-log.txt 2>&1`




