#! /usr/bin/env python3.7
# -*- coding: utf-8

import pymongo
import time
from bluepy.btle import Scanner
import time

DATABASE_NAME_DISTANT = "rssi_database.db"
DATABASE_NAME_DISTANT_ = "rssi_database"
TABLE_PINGS_NAME = "rssis_table"
TIMEOUT_DISTANT_DATABASE = 5000  # ms
SCANNING_TIME = 5.0

class DataBaseMongoDB:

    def __init__(self, _database_name):
        connect_string = "mongodb://root:root@cluster0-shard-00-00.sdbck.mongodb.net:27017," \
                         "cluster0-shard-00-01.sdbck.mongodb.net:27017,cluster0-shard-00-02.sdbck.mongodb.net:27017/" \
                         + _database_name + "?ssl=true&replicaSet=atlas-xgr1o7-shard-0&authSource=admin&retryWrites" \
                                            "=true&w=majority"
        self._connection = pymongo.MongoClient(connect_string, serverSelectionTimeoutMS=TIMEOUT_DISTANT_DATABASE)
        self._distant_db = self._connection[DATABASE_NAME_DISTANT_]

    def fetch(self):
        _cursor = self._distant_db[TABLE_PINGS_NAME].find()
        result = [doc for doc in _cursor]
        return result

    def persist(self, _query: str):
        self._distant_db[TABLE_PINGS_NAME].insert_one(_query)
        return True

    def build_query(self, device_addr, rssi, datetime):
        _query = {'id': None, 'device_addr': device_addr, 'rssi': rssi, 'datetime': datetime}
        return _query


if __name__ == "__main__":
    # initiate database
    distantMongoDB = DataBaseMongoDB(DATABASE_NAME_DISTANT)
    # take measurement
    scanner = Scanner()
    devices = scanner.scan(SCANNING_TIME)
    if not devices:
        print("no devices found")
    # loop over devices and write
    for device in devices:
        print("device [{}] at rssi [{}] name [{}]".format(device.addr, device.rssi, device.getValueText(9)))
        query = distantMongoDB.build_query(device.addr, device.rssi, time.time())
        distantMongoDB.persist(query)
    a = distantMongoDB.fetch()

