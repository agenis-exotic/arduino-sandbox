# Basic IP-writer for raspberry

ce petit script à mettre en crontab reboot permet d'écrire dans un fichier l'adresse IP du raspberry quand il est connecté au wifi.
La commande standard qui est la suivante ne fonctionne pas sur raspberry et renvoie toujours 127.0.0.1:

`import socket`

`socket.gethostbyname(socket.gethostname())`

on remplace donc par une fonction un peu plus evoluee (workaround)

on écrit l'IP à la ligne dans le fichier

`@reboot /usr/bin/python3 /home/pi/Documents/ipwriter.py &`

Note: sur un PyCom par ex WiPy on peut taper:

`import network; print(network.WLAN().ifconfig()[0])`
