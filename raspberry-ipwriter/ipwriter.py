import socket
import time


def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except Exception:
        IP = 'error'
    finally:
        s.close()
    return IP
for _ in range(20):
    with open("/home/pi/Documents/ip.txt", "a+") as con:
        con.write(get_ip() + "\n")
    time.sleep(10)

    
