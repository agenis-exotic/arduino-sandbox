#! /usr/bin/env python3.7
# -*- coding: utf-8
import time
import os
import RPi.GPIO as GPIO
import logging
import atexit


def setup():
    # remove previous logs
    os.remove("distance_log.log")
    global Trig
    global Echo
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    Trig = 23         # Entree Trig du HC-SR04 branchee au GPIO 23
    Echo = 24         # Sortie Echo du HC-SR04 branchee au GPIO 24
    GPIO.setup(Trig, GPIO.OUT)
    GPIO.setup(Echo, GPIO.IN)
    GPIO.output(Trig, False)
    logging.basicConfig(filename='distance_log.log', level=logging.INFO)
    # enable LED blinking
    os.system('sudo sh -c "echo none >/sys/class/leds/led0/trigger"')


def mesure(time_interval=0.1, print_to_console=True, tolerance=50):
    current_distance = 0
    while True:
        time.sleep(time_interval)       # On la prend toute les 0.1 seconde
        os.system('sudo sh -c "echo heartbeat >/sys/class/leds/led0/trigger"')
        GPIO.output(Trig, True)
        time.sleep(0.00001)
        GPIO.output(Trig, False)
        while GPIO.input(Echo) == 0:  # Emission de l'ultrason
            debut_impulsion = time.time()
        while GPIO.input(Echo) == 1:   # Retour de l'Echo
            fin_impulsion = time.time()
        distance = round((fin_impulsion - debut_impulsion) * 340 * 100 / 2, 1)  # Vitesse du son = 340 m/s
        # on se reveille si il y a un gros changement de distance:
        if abs(current_distance - distance) > tolerance and distance < 1000:
            current_distance = distance
            # log into file:
            text_to_log = f'distance: {current_distance} @ {fin_impulsion}'
            logging.info(text_to_log)
            if print_to_console: print(text_to_log)
            # blink LED green
            os.system('sudo sh -c "echo heartbeat >/sys/class/leds/led0/trigger"')


def dispose():
    GPIO.cleanup()


if __name__ == "__main__":
    setup()
    mesure(print_to_console=True, tolerance=40)
    atexit.register(dispose)
