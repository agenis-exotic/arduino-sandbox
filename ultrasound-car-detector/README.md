Projet simple constitué d'un seul script python. Matériel:
- raspberry pi 3B+
- HC-SR04 ultrasonic range finder
- some jumper wires
- deux résistances de ?? ohms

branchements:
- Trig: GPIO23
- Echo: GPIO24

le programme se lance au démarrage (à mettre dans le crontab), logue dans un fichier chaque mesure de distance à un intervalle de temps choisi. 
Un mini algorithme qui n'enregistre que les mesures s'écartant de plus de X de la valeur précédente;

Le programme utilise la LED native du raspberry pour la faire clignoter s'il voit un changement de distance.

![device](images/Sans_titre.png)
