# Capteur de distance laser couplé à une LED plus ou moins lumineuse.

Matériel:
- 1 VL53L01X de chez CQRobot
- 1 Arduino nano
- 1 boitier + 3 piles AAA 1.5V
- 1 Boost Converter Step-up Voltage Regulator
- 1 led 
- 1 resistance 220 Ohms
- 1 breadboard
- jumper wires

connections:
- LED a D3 (D3 est compatible PWM)
- SCL sur A5
- SDA sur A4

commentaires: on met un map() entre la lecture du laser et le brightness. 
pour linéariser la brightness on crée une lookuptable qui reproduit une fonction racine ou log.

l'alimentation de la nano est faite via la pin VIN, et un ensemble pile + regulateur tension boost (qu'on met à 9Volt), sur le VIN ca accepte un range entre 6V et 14V je crois. 

Photo:

![device](images/device.png)
