# log de temperature sur mongodb

Un petit script sur mon raspberry pi Zero qui loggue la température interne CPU et l'envoie sur mon cloud mongodb.
Le log température se fait simplement avec la librairie gpiozero.

script à mettre en crontab:
`*/2 * * * * /usr/bin/python3 /home/pi/Documents/tempwriter.py &`
