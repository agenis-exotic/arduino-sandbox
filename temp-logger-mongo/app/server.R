library(shiny)

function(input, output, session) {
  
  datas = reactiveValues(ping_table = NULL,
                         nrow=3)
  
  output$StatsTable = renderUI({
    h3("Table de statistiques")
  })
  
  # output$plot = renderPlotly({
  #   x <- c(1:100)
  #   random_y <- rnorm(100, mean = 0)
  #   data <- data.frame(x, random_y)
  #   fig <- plot_ly(data, x = ~x, y = ~random_y, type = 'scatter', mode = 'lines')
  #   return(fig)
  # })
  
  output$plot = renderPlotly({
    if (is.null(datas$ping_table)) return()
    #ggplot(data=datas$ping_table) + aes(x=datetime_display, ymin=0, ymax=result) + geom_ribbon()
    
    fig <- plot_ly(datas$ping_table, x = ~datetime_display)
    fig <- fig %>% add_lines(y = ~temp, name = "TEMPERATURE CPU")
    fig <- fig %>% layout(
      title = "Temperature Raspberry",
      xaxis = list(
        rangeselector = list(
          buttons = list(
            list(
              count = 3,
              label = "3 mo",
              step = "month",
              stepmode = "backward"),
            list(
              count = 6,
              label = "6 mo",
              step = "month",
              stepmode = "backward"),
            list(
              count = 1,
              label = "1 yr",
              step = "year",
              stepmode = "backward"),
            list(
              count = 1,
              label = "YTD",
              step = "year",
              stepmode = "todate"),
            list(step = "all"))),
        
        rangeslider = list(type = "date")),
      
      yaxis = list(title = "Degres Celcius"))
    
    fig
    
    
  })
  
  output$routeSelect <- renderUI({
    selectInput("routeNum", "Device", choices = unique(datas$ping_table$device), selected = "fleurda")
  })
  
  output$timeSinceLastSync = renderUI({
    paste0("Mis a jour ", 60, " seconds ago")
  })
  
  observeEvent(input$update_database, handlerExpr = {
    datas$nrow = datas$nrow +1
    temp = con$find(query = "{}")
    #temp[temp$wifi_name != "FLEURDA", ]$result <- 0
    temp$datetime_display = as.POSIXct(temp$datetime, origin = "1970-01-01", tz = "Europe/Paris")
    datas$ping_table = temp
  }, ignoreNULL = FALSE, ignoreInit = FALSE)
  
  
  
}