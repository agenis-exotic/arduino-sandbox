## Comprehensive distance sensor with OLED display, battery, laser, and signal processing

On propose le schéma de cablage suivant:

![](images/vl53l01x-to-oled_bb.png)

Avec les corrections suivantes:
- la diode est un laser 5V, connecté sur les pins 1 (-) et 3 (S)
- le step down est en fait un step up linéaire avec réglage à 9V en sortie
- le capteur de distance est un vl53l01x
- l'alimentation est composée de 3 piles AAA.
