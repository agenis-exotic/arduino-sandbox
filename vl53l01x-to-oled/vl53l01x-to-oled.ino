// VL53L1X Distance Sensor from CQRobot: http://www.cqrobot.wiki/index.php/VL53L1X_Distance_Sensor

//// Libraries
#include <Arduino.h>
#include <U8g2lib.h>
#include <Wire.h>
#include "VL53L1X.h"
//// instanciation classes
VL53L1X Distance_Sensor;
U8G2_SSD1306_128X64_NONAME_1_HW_I2C u8g2(U8G2_R0, /* reset=*/ U8X8_PIN_NONE);
//// define variables 
int distance = 0;
int Button_stand_off_mode;
int State_time_smooth_mode=1; 
const byte BUTTON_TIME_SMOOTH_MODE_PIN = 2; //D2
const byte BUTTON_DISTANCE_SIDE_SELECTOR_PIN = 3; //D3
const byte LASER_1_PIN = 4; //D4
const int INTERVAL_BLINK_LASER1 = 200;
String output;
#define BUF_SIZE 10
float array_calculate_avg(int*buf, int len);
int buf[BUF_SIZE] = {0};
int buf_index = 0;
int value_to_display = 0;
int abrupt_change_of_distance_threshold = 100;
int threshold_for_value_stability_low = 10;
int threshold_for_value_stability_medium = 5;
int threshold_for_value_stability_high = 2;
String displayText = "distance en mm";
bool ActivateRollingAvg = false;
const float distance_front_to_sensor_mm = 5; 
const float distance_sensor_to_rear_mm = 30; 
// gerer compteur de temps en millis au lieu de delay
unsigned long previous_time = 0;
unsigned long millis_interval = 20;

//// fonction pour calculer la moyenne d'un array d'entiers (dist en mm)
float array_calculate_avg(int*buf, int len){
  int sum = 0;
  for (int i = 0; i<len; i++){
    sum += buf[i];
  }
  return ((float)(sum) / (float)(len));
}

// fonction pour blinker le laser1
void task_blink_laser1() {
  static unsigned long previousMillisLaser1 = 0;
  static byte etatLaser1 = LOW;
  if(millis() - previousMillisLaser1 >= INTERVAL_BLINK_LASER1) {
    previousMillisLaser1 = millis();
    etatLaser1 = !etatLaser1;
    digitalWrite(LASER_1_PIN, etatLaser1);
  }
}

//// fonction pour calculer l'écart type d'un array d'entiers (necessite d'avoir calculé la moyenne d'abord)
float array_calculate_std(int * buf, int len, float avg) {
  float total = 0;
  for (int i = 0; i < len; i++) {
    total += pow((float) buf[i] - avg, 2);
  }
  float variance = total/(float)len;
  float stdDev = sqrt(variance);
  return stdDev;
}

//// FONCTION SETUP
void setup()
{
  pinMode(BUTTON_TIME_SMOOTH_MODE_PIN, INPUT_PULLUP);
  pinMode(BUTTON_DISTANCE_SIDE_SELECTOR_PIN, INPUT_PULLUP);
  pinMode(LASER_1_PIN, OUTPUT);
  Wire.begin();
  Wire.setClock(400000);// use 400 kHz I2C
  u8g2.begin();
  u8g2.enableUTF8Print();
  Serial.begin(115200);
  Distance_Sensor.setTimeout(500);
  if (!Distance_Sensor.init())
  {
    Serial.println("Failed to initialize VL53L1X Distance_Sensor!");
    while (1);
  }
  //// parametres du capteur distance (micro seconde le budget)
  Distance_Sensor.setDistanceMode(VL53L1X::Long);
  Distance_Sensor.setMeasurementTimingBudget(50000);
  Distance_Sensor.startContinuous(50);
}

//// BOUCLE PRINCIPALE
void loop()
{  
  // allumer le laser
  task_blink_laser1();
  
  //// selon le choix du bouton on change de mode, avec la fonction millis
  //// on incremente le bouton, le delay pour eviter appuis multiples
  if(digitalRead(BUTTON_TIME_SMOOTH_MODE_PIN)==LOW){
    if(State_time_smooth_mode<3){
      State_time_smooth_mode+=1;
    } else {
      State_time_smooth_mode=1;
    }
    delay(100);
    // on reset le compteur de millis comme si le temps max était déjà passé:
    previous_time = millis()-1000;
  }
  switch(State_time_smooth_mode){
    case 1:
      displayText = "dist(mm)/avg(10)";
      ActivateRollingAvg = true;
      //delay(20);
      millis_interval = 20;
      break;
    case 2:
      displayText = "dist(mm)/raw/live";
      ActivateRollingAvg = false;
      //delay(20);
      millis_interval = 20;
      break;
    case 3:
      displayText = "dist(mm)/raw/1s.";
      ActivateRollingAvg = false;
      //delay(950);
      millis_interval = 950;
      break;
    }

    //// Tout le reste du programme est sous condition millis() milli_interval:
    if (millis() - previous_time > millis_interval){
      // reset le compteur de temps
      previous_time = millis();
      ////////////// reste du programme:


        //// lecture de la nouvelle mesure
        Distance_Sensor.read();
        distance = Distance_Sensor.ranging_data.range_mm - distance_front_to_sensor_mm;
        if (digitalRead(BUTTON_DISTANCE_SIDE_SELECTOR_PIN)==LOW){
          // choix du mode de calcul distance stand off.
          distance = distance + distance_sensor_to_rear_mm;
        }      
        //// calculer les statistiques sur l'array
        float avg = array_calculate_avg(buf, BUF_SIZE);
        float std = array_calculate_std(buf, BUF_SIZE, avg);
        
        //// si la mesure est trop différente, on la prend comme nouvelle reference
        if (abs(distance - (int) avg)>abrupt_change_of_distance_threshold){
          for(int j=0;j<BUF_SIZE;j++){
                buf[j] = distance;
              }
          std = 1000;
        } else {
          //// la nouvelle mesure ecrase la valeur de l'array n-10
          if (BUF_SIZE == buf_index){
            buf_index = 0;
          }
          buf[buf_index++] = distance;
        }
        
        
        //// switch selon le choix bouton.
        if (ActivateRollingAvg){    
          value_to_display = (int) avg;
        } else {
          value_to_display = (int) distance;
        }
        //// on envoie la valeur de distance brute sur le serial print
        Serial.println(value_to_display);
        //// transformer valeur en string avec leading spaces:
        if (value_to_display<10){
          output = "    " + String(value_to_display);
        }
        else if (value_to_display<100){
          output = "   " + String(value_to_display);
        }
        else if (value_to_display<1000){
          output = " " + String(value_to_display);
        }
        else if (value_to_display<10000){
          output = "" + String(value_to_display);
        }
        else {
          // si erreur
          output = "----";
        }
      
        //// on prepare le display LCD
        u8g2.firstPage();
        do {
          u8g2.setFont(u8g2_font_8x13_mf);
          u8g2.drawStr(0,10, displayText.c_str());
          u8g2.setFont(u8g2_font_logisoso42_tn);
          u8g2.drawStr(0,65,output.c_str());
          //// afficher la stabilité sous forme de points
          u8g2.setFont(u8g2_font_logisoso50_tn);
          if (std < threshold_for_value_stability_high) u8g2.drawStr(110,30,".");
          if (std < threshold_for_value_stability_medium) u8g2.drawStr(110,45,".");
          if (std < threshold_for_value_stability_low) u8g2.drawStr(110,60,".");
        } while ( u8g2.nextPage() );



      
      
    }

    
}
