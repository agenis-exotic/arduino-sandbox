import pycom
import time
import ubinascii
from network import Bluetooth
bluetooth = Bluetooth()

bluetooth.start_scan(10)
while bluetooth.isscanning():
    # il lit un device qu'il recoit
    adv = bluetooth.get_adv()
    # si le device n'est pas vide:
    if adv:
        # si le device est bien de la marque pycom:
        try:
            manufacturer = bluetooth.resolve_adv_data(adv.data, Bluetooth.ADV_MANUFACTURER_DATA).decode()
            if (manufacturer == "pycom"):
                print(adv.rssi)
        except:
            pass

    time.sleep(0.1)

print("starting advertise..")
bluetooth.set_advertisement(name="advert-marc2-pycom_noscotch", manufacturer_data="pycom")
bluetooth.advertise(True)
