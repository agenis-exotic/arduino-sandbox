# 7 digit display of air quality

matériel:
- MQ135 quality sensor
- arduino nano
- PCB
- alimentation USB
- 7 segment display common-Cathode
- librairies: MQUnifiedsensor (officielle) et SevSeg (téléchargée ici: https://github.com/DeanIsMe/SevSeg )


Schéma ici + capteur air mis sur le pin A5.
https://www.circuitbasics.com/wp-content/uploads/2017/05/Arduino-7-Segment-Display-4-Digit-Temperature-Display.png

Image du device:
![device](images/device.png)
