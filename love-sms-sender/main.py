#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests
import random
import datetime
import time

MY_WEBHOOK_KEY = "dUpx1ZRSWK4z-V_clvkLkP"
LOWER_BOUND_TIME = 9
UPPER_BOUND_TIME = 16


class LoveMessages:
    def __init__(self):
        self.messages = ["je t'aime", "t'aime", "bonne jorunée mon amour", "comment ca va?",
                         "je pense à toi", "je pense à toi", "bnne journée", "<3<3<3", "<3",
                         "amour", "tu es toute ma vie", "EDT", "edt", "Je t'aime", "coucou ma chérie"]
        self.size = len(self.messages)

    def random(self):
        random_message = self.messages[random.randint(0, self.size-1)]
        if bool(random.getrandbits(1)):
            random_message = random_message.capitalize()
        return random_message


def is_it_the_right_time():
    if (datetime.datetime.now().hour > LOWER_BOUND_TIME) and (datetime.datetime.now().hour+1 <= UPPER_BOUND_TIME) and ((datetime.datetime.today().weekday()+1) in [1,2,4,5]):
        return True
    else:
        return False


def send_sms(value1, value2, value3, endpoint="love"):
    body = {"value1": value1, "value2": value2, "value3": value3}
    url = "https://maker.ifttt.com/trigger/{}/with/key/{}".format(endpoint, MY_WEBHOOK_KEY)
    requests.post(url, data=body)
    print("Sending sms at {}".format(datetime.datetime.now()))


def run_loop():
    while True:
        if is_it_the_right_time():
            send_sms(LoveMessages().random(), "", "", "love")
            #time.sleep(random.randint(1, 15))
            time.sleep(random.randint(20*3600, 24*3600))
        else:
            #send_sms(LoveMessages().random(), "", "", "love")
            #time.sleep(random.randint(15, 30))
            # go to sleep for around 12 hours (between 8 and 14).
            time.sleep(random.randint(4*3600, 9*3600))


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    run_loop()
