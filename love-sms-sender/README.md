# Love SMS sender

Ce programme envoie des SMS d'amour aléatoires.

Il utilise le service IFTTT de trigger avec un webhook, couplé à l'appli IFTTT sur mon android.

Les SMS viennent sont donc émis par moi.

## Installation du raspberry

Ce montage utilise:
- un raspberry pi Zero W
- une carte SD 16Go bas de gamme

On installe sur la carte SD l'OS via l'outil raspberry pi imager: 
- Raspberry PI OS Lite (32-bit) du 4 mars 2021 linux kernel 5.10.17

Ensuite on se connecte via un écran (il faut un adaptateur micro-hdmi et micro-usb vers usb femelle), avec les logins par default (pi / raspberry). Ensuite les étapes sont les suivantes:
`sudo raspi-config`
Dans options d'interface, activer le SSH. Dans système, connecter le wifi a son réseau. Puis chercher sur le modem l'adresse IP locale du raspberry et s'y connecter via putty. Ensuite on change le nom public du raspberry et le mot de passe par défaut pour mettre un nouveau et rebooter. Ensuite exécuter:
`sudo apt-get update`


## paramétrage

Il faut déplacer les fichiers .service et .timer vers:
`/etc/systemd/system/`

ou les créer en `sudo nano`.

Ensuite on les démarre comme ceci:

```
sudo systemctl enable love-sms.service
sudo systemctl enable love-sms.timer
sudo systemctl daemon-reload
sudo systemctl start love-sms.timer
sudo systemctl enable love-sms.service
sudo reboot
```

