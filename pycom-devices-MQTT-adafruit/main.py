# main.py -- put your code here!
import pycom
from adafruit import *

client = MQTTClient(AIO_CLIENT_ID, AIO_SERVER, AIO_PORT, AIO_USER, AIO_KEY)

client.set_callback(sub_cb)
client.connect()
client.subscribe(AIO_MAXRANGE_FEED)
print("Connected to %s, subscribed to %s topic" % (AIO_SERVER, AIO_MAXRANGE_FEED))
pycom.rgbled(0x00ff00) #green

# sending loop
try:
    while 1:
        client.check_msg()
        send_random(client)
finally:
    client.disconnect()
    client = None
    pycom.rgbled(0x00022)
    print("connexion arrÃªtÃ©e")
