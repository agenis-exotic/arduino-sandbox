# main.py -- put your code here!
from network import WLAN
import time
import pycom
from umqtt import MQTTClient
import ubinascii
import machine
import micropython


# settings
RAMDOM_INTERVAL = 5000 #ms
last_random_sent_ticks = 0 #ms
max_random_value = 100 # global
# WIFI_SSID = #wifi name
# WIFI_PASS = #mypass already in json file at root
AIO_SERVER = "io.adafruit.com"
AIO_PORT = 1883
AIO_USER = "agenis"
AIO_KEY = "aio_bbAn91osyV2aJWMQfcBEKUNTomoV"
AIO_CLIENT_ID = ubinascii.hexlify(machine.unique_id())
AIO_RANDOM_FEED = "agenis/feeds/testfeed1"
AIO_MAXRANGE_FEED = "agenis/feeds/maxrange"
# LEDS
pycom.heartbeat(False)
time.sleep(0.5)
pycom.rgbled(0xff0000)
time.sleep(0.5)

# check WIFI
wlan = WLAN()
while not wlan.isconnected():
    machine.idle()
print("connected to wifi")
pycom.rgbled(0xffd7000) # Status orange: partially working

# Function to respond to messages from Adafruit IO
def sub_cb(topic, msg):          # sub_cb means "callback subroutine"
    global max_random_value
    print("set maxrange to {0}".format(msg))          # Outputs the message that was received. Debugging use.
    max_random_value = int(msg)
    # if msg == b"ON":             # If message says "ON" ...
    #     pycom.rgbled(0xffffff)   # ... then LED on
    # elif msg == b"OFF":          # If message says "OFF" ...
    #     pycom.rgbled(0x000000)   # ... then LED off
    # else:                        # If any other message is received ...
    #     print("Unknown message") # ... do nothing but output that it happened.

def random_integer(upper_bound=100):
    return machine.rng() % upper_bound


def send_random(client):
    global last_random_sent_ticks
    global RAMDOM_INTERVAL
    # is it time to send?
    if ((time.ticks_ms() - last_random_sent_ticks) < RAMDOM_INTERVAL):
        return;

    some_number = random_integer(upper_bound=max_random_value)
    print("Publishing: {0} to {1} ... ".format(some_number, AIO_RANDOM_FEED), end='')
    try:
        client.publish(topic=AIO_RANDOM_FEED, msg = str(some_number))
    except Exception as e:
        print("FAILED")
    finally:
        last_random_sent_ticks = time.ticks_ms()
        for _ in range(5): # blink
            pycom.rgbled(0x00ff00) #green
            time.sleep(0.05)
            pycom.rgbled(0x000000) #green
            time.sleep(0.05)
        pycom.rgbled(0x000000) # off


# MAIN SCRIPT posted to main.py
