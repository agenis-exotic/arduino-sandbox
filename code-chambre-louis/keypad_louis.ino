#include <Keypad.h>
// #include "pitches.h"

const byte ROWS = 4; 
const byte COLS = 4; 

const String password = "C5D";
String input_password;

char hexaKeys[ROWS][COLS] = {
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
};

byte rowPins[ROWS] = {9, 8, 7, 6}; 
byte colPins[COLS] = {5, 4, 3, 2}; 

Keypad customKeypad = Keypad(makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS); 

void setup(){
  Serial.begin(9600);
  input_password.reserve(32); // maximum input characters is 33, change if needed
}


void loop(){
  
  char touche = customKeypad.getKey();

  if (touche){
    Serial.println(touche);
    if (touche == '*'){
      input_password = ""; // supprimer le mot de passe qui est en train d'etre écrit
      tone(11, 110, 100);
      delay(100);
    } else if (touche == '#'){
    if (password == input_password){
      Serial.println("c'est bon");
        tone(11, 262, 1000);
        delay(500);
        noTone(500);
        tone(11, 440, 1000);
        delay(500);
      
    } else {
      Serial.println("c'est faux");
        tone(11, 220, 1000);
        delay(500);
        noTone(500);
        tone(11, 220, 1000);
        delay(500);
      }
      input_password = "";
  } else {
    input_password = input_password + touche;
    tone(11, 880, 100);
    delay(100);
     }
  }
 }
