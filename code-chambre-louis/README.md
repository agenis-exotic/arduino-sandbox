Un device pour taper un code. Si le code est correct ca sonne une musique, sinon une autre. 
Matériel:
- arduino uno
- keypad 4x4
- buzzer passif
- boite plastique
- banque d'énergie usb
- interrupteur usb

photo:
![image du device](images/device.png)
