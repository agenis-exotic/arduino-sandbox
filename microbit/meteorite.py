def nouvelle_meteorite():
    global meteorite
    meteorite = game.create_sprite(randint(0, 4), 0)
    for index in range(4):
        basic.pause(500)
        if meteorite.get(LedSpriteProperty.Y) < 3:
            meteorite.change(LedSpriteProperty.X,
                Math.constrain(vaisseau.get(LedSpriteProperty.X) - 2 - (meteorite.get(LedSpriteProperty.X) - 2),
                    -1,
                    1))
        meteorite.change(LedSpriteProperty.Y, 1)

def on_button_pressed_a():
    vaisseau.move(-1)
input.on_button_pressed(Button.A, on_button_pressed_a)

def on_button_pressed_b():
    vaisseau.move(1)
input.on_button_pressed(Button.B, on_button_pressed_b)

def perdu():
    basic.show_leds("""
        . . . . .
        . . # . .
        . # . # .
        . . # . .
        . . . . .
        """)
    basic.pause(2000)
meteorite: game.LedSprite = None
vaisseau: game.LedSprite = None
vaisseau = game.create_sprite(2, 4)
nouvelle_meteorite()
basic.pause(1000)

def on_forever():
    if meteorite.is_touching(vaisseau):
        perdu()
    if meteorite.is_touching_edge():
        meteorite.delete()
        basic.pause(500)
        nouvelle_meteorite()
basic.forever(on_forever)
