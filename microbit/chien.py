def repos():
    basic.show_leds("""
        . . . # #
        . . . # .
        # . . # .
        . # # # .
        . # . # .
        """)

def on_button_pressed_a():
    global sauter, regarde, marcher
    sauter = "non"
    regarde = "non"
    marcher = "oui"
input.on_button_pressed(Button.A, on_button_pressed_a)

def regarder():
    basic.show_leds("""
        . . # # .
        . . . # .
        # . . # .
        . # # # .
        . # . # .
        """)
    basic.pause(100)
    basic.show_leds("""
        . . . # #
        . . . # .
        # . . # .
        . # # # .
        . # . # .
        """)
    basic.pause(100)
def fonction_sauter():
    basic.show_leds("""
        . . . # .
        # . . # .
        . # # # .
        . # . # .
        . . . . .
        """)
    basic.pause(100)
    basic.show_leds("""
        . . . # #
        . . . # .
        # . . # .
        . # # # .
        . # . # .
        """)
    basic.pause(100)
def fonction_marcher():
    basic.show_leds("""
        . . . # #
        . . . # .
        # . . # .
        . # # # .
        # . # . .
        """)
    basic.pause(100)
    basic.show_leds("""
        . . . # #
        . . . # .
        # . . # .
        . # # # .
        . # . # .
        """)
    basic.pause(100)

def on_button_pressed_ab():
    global marcher, regarde, sauter
    marcher = "non"
    regarde = "non"
    sauter = "oui"
input.on_button_pressed(Button.AB, on_button_pressed_ab)

def on_button_pressed_b():
    global sauter, marcher, regarde
    sauter = "non"
    marcher = "non"
    regarde = "oui"
input.on_button_pressed(Button.B, on_button_pressed_b)

regarde = ""
sauter = ""
marcher = ""
repos()
marcher = "non"
sauter = "non"
regarde = "non"

def on_forever():
    while marcher == "oui":
        fonction_marcher()
    while sauter == "oui":
        fonction_sauter()
    while regarde == "oui":
        regarder()
basic.forever(on_forever)
