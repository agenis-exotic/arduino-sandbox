#include <math.h>

// LEDs
int DIN_PIN = 12;
int CS_PIN  = 11;
int CLK_PIN = 10;
// microphone
int MIC_A0_PIN = 2;
int micro_value; 
int max_micro_value=0; 

int ARRAY[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
int number_complete_lines;

//////////////////////////////////////////////
// fonctions pour faire tourner la matrice LED
//////////////////////////////////////////////

void write_pix(int data){
  digitalWrite(CS_PIN, LOW);
  for (int i=0; i<8; i++){
    digitalWrite(CLK_PIN, LOW);
    digitalWrite(DIN_PIN, data & 0x80); // masquage de la donnée
    data = data << 1; // on decale tous les bits vers la gauche
    digitalWrite(CLK_PIN, HIGH);
  }
}
void write_line(int address, int data){
  digitalWrite(CS_PIN, LOW);
  write_pix(address);
  write_pix(data);
  digitalWrite(CS_PIN, HIGH);
}
void write_matrix(int tab[8]){
  for (int i=0; i<8; i++) write_line(i+1, tab[i]);
}
void init_MAX7219(void){
  write_line(0x09, 0x00); // decoding BCD
  write_line(0x0A, 0x01); // brightness
  write_line(0x0B, 0x07); // scanlimite: 8leds
  write_line(0x0C, 0x01); // power down mode: 0, normal mode:1
  write_line(0x0F, 0x00); // test display 1: EOT, display:0
}
void clear_matrix(void){
  const int clean[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
  write_matrix(clean);
}
int intToHex(int x){
  switch(x){
    case 0: return 0x01; break; // LED sur la 1ere case = 1
    case 1: return 0x02; break; // LED sur la 2ere case = 2
    case 2: return 0x04; break; // LED sur la 3ere case = 4
    case 3: return 0x08; break; // LED sur la 4ere case = 8
    case 4: return 0x10; break; // LED sur la 5ere case = 16
    case 5: return 0x20; break; // LED sur la 6ere case = 32 
    case 6: return 0x40; break; // LED sur la 7ere case = 64
    case 7: return 0x80; break; // LED sur la 8ere case = 128
  }
}
//////////////////////////////////////////////
// fin
//////////////////////////////////////////////

// fonction pour remplir la matrice LED avec un nombre distirbué sur toutes les LED.
int map_matrix(int x){
  number_complete_lines = round(x/8); // si nombre 160 => 10 leds => number_ = 1
  for (int i=0; i<8; i++){
    if (i<number_complete_lines){
      ARRAY[i] = 0xff; // ligne complete = 255
    } else if (i == number_complete_lines){
      int incr[9] = {0x00, 0x01, 0x03, 0x07, 0x0f, 0x1f, 0x3f, 0x7f, 0xff};
      ARRAY[i] = incr[x-number_complete_lines*8];
    } else { // les autres lignes
      ARRAY[i] = 0x00;
    }
  }
  return ARRAY; // renvoie un array 8x8
}

void setup() {
  pinMode(CS_PIN, OUTPUT);
  pinMode(CLK_PIN, OUTPUT);
  pinMode(DIN_PIN, OUTPUT);
  pinMode(MIC_A0_PIN, INPUT);
  delay(50);
  init_MAX7219();
  clear_matrix();
  Serial.begin(9600);
}

void loop() {
  micro_value = analogRead(MIC_A0_PIN);
  if (micro_value > max_micro_value){
    max_micro_value = micro_value;
  }
  Serial.println(max_micro_value);
  write_matrix(map_matrix(map(max_micro_value, 500, 1500, 0, 64))); 
  delay(50);
}
