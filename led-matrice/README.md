un projet tout simple de visualisation du niveau sonore d'un micro, qui utilise:
- arduino nano
- micro arduino (sound sensor module 4 pins)
- matrice de leds 8*8 max7219
- cables dupont
- un mini breadboard si besoin

la valeur analogique du micro est envoyée sur un pin, et est affichée sur la matrice de leds comme un empilement de points allumés de plsu en plus grand. On utilise aucune librairie, les fonctions de la matrice led sont codées en dur.

Le "trick" est de convertir la valeur analogique en la mappant vers [0, 64];

les enfants adorent crier dedans..

![device](images/device.png)
