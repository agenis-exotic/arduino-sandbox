/*
  Rotary Encoder with Servo Motor Demo
  rot-encode-servo-demo.ino
  Demonstrates operation of Rotary Encoder
  Positions Servo Motor
  Displays results on Serial Monitor
  DroneBot Workshop 2019
  https://dronebotworkshop.com
*/
// adapté pour les pins de la carte Arduino Nano
 
 // Include the Servo Library
 #include <Servo.h>
 
 // Rotary Encoder Inputs
 #define inputCLK 4
 #define inputDT 5

 int feuvert = 10;
 int feuorange = 11;
 int feurouge = 12;
 
 // Create a Servo object
 Servo myservo;
 
 int counter = 20; 
 int currentStateCLK;
 int previousStateCLK; 
 
 void setup() { 
   myservo.write(20);
   // Set encoder pins as inputs  
   pinMode (inputCLK,INPUT);
   pinMode (inputDT,INPUT);
   // leds
   pinMode(feuvert, OUTPUT);
   pinMode(feuorange, OUTPUT);
   pinMode(feurouge, OUTPUT);
   
   // Setup Serial Monitor
   Serial.begin (9600);
   
   // Attach servo on pin 9 to the servo object
   myservo.attach(9);
   
   // Read the initial state of inputCLK
   // Assign to previousStateCLK variable
   previousStateCLK = 1; //digitalRead(inputCLK);
  digitalWrite(feurouge, HIGH);
 } 
 
 void loop() {
  
  // Read the current state of inputCLK
  currentStateCLK = digitalRead(inputCLK);
    
   // If the previous and the current state of the inputCLK are different then a pulse has occured
   if (currentStateCLK != previousStateCLK){ 
       
     // If the inputDT state is different than the inputCLK state then 
     // the encoder is rotating counterclockwise
     if (digitalRead(inputDT) != currentStateCLK) { 
       counter = counter + 5;

      
     } else {
       // Encoder is rotating clockwise
       counter = counter - 5;

       
     }
        if (counter<=20){
        counter=20;
         digitalWrite(feurouge, HIGH);
         digitalWrite(feuorange, LOW);
         digitalWrite(feuvert, LOW);
       }
        if (counter>=90){
        counter=90;
        digitalWrite(feuorange, LOW);
        digitalWrite(feuvert, HIGH);
       }
      if (counter < 100 && counter > 20){
        digitalWrite(feuorange, HIGH); 
        digitalWrite(feurouge, LOW);
        digitalWrite(feuvert, LOW);
      }

     
     // Move the servo
     myservo.write(counter);
     //delay(100);
     //myservo.write(90);
      
     Serial.print("Position: ");
     Serial.println(counter);
   } 
   // Update previousStateCLK with the current state
   previousStateCLK = currentStateCLK; 
 }