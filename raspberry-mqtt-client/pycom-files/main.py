import pycom
from mqtt import MQTTClient
from network import WLAN
import machine
import time
import ubinascii
client_id = ubinascii.hexlify(machine.unique_id())

def sub_cb(topic, msg):
   print(msg)

client = MQTTClient(client_id, '192.168.0.21',user="pi", password="pi", port=1883)

client.set_callback(sub_cb)
client.connect()
client.subscribe(topic="marco")

########bkluetooth setup.
from network import Bluetooth
bluetooth = Bluetooth()
print("starting advertise..")
bluetooth.set_advertisement(name="advert-marc2-pycom_noscotch", manufacturer_data="pycom")
bluetooth.advertise(True)
bluetooth.start_scan(-1)
while bluetooth.isscanning():
    # il lit un device qu'il recoit
    adv = bluetooth.get_adv()
    # si le device n'est pas vide:
    if adv:
        # si le device est bien de la marque pycom:
        try:
            manufacturer = bluetooth.resolve_adv_data(adv.data, Bluetooth.ADV_MANUFACTURER_DATA).decode()
            if (manufacturer == "pycom"):
                print(adv.rssi)
                client.publish(topic="rssi", msg=str(adv.rssi))
        except:
            pass
    time.sleep(1)

