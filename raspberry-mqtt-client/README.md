## rasperry MQTT client with mosquitto

Follow instructions! Tested on my Raspberry Pi Zero with Buster.

`sudo apt-get update`

`sudo apt install -y mosquitto mosquitto-clients`

start at boot

`sudo systemctl enable mosquitto.service`

run on background

`mosquitto -d`

subscribe and listen:

`mosquitto_sub -d -t marco`

publish:

`mosquitto_pub -d -t marco -m "Hello world!"`

securiser un mot de passer pour un user "pi" (mdp est aussi "pi")

`cd /etc/mosquitto `

`sudo mosquitto_passwd -c p1.txt pi`

ouvrir le fichier suivant, et ajouter à la fin:       allow_anonymous false

`sudo nano mosquitto.conf`

redemarrer le service

`sudo service mosquitto restart`

A présent il faut faire ca pour observer:

`mosquitto_sub -d -t marco -u pi -P pi`

